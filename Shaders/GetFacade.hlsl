#ifndef GETFACADE_3
#define GETFACADE_3


void GetFacade_float(float batchId, float2 uv,
						 UnityTexture2D facade0, UnityTexture2D facade1, UnityTexture2D facade2, UnityTexture2D facade3,
						 UnityTexture2D facade4, UnityTexture2D facade5, UnityTexture2D facade6, UnityTexture2D facade7,
					     out float4 color)
{
    int b = (int) floor(batchId + 0.5f);
    if (b < 0)
        b = -b;
    b %= 8;
    color = float4(1, 1, 1, 1);
    switch (b)
    {
        case 0:
            color = tex2D(facade0, uv);
            break;
        case 1:
            color = tex2D(facade1, uv);
            break;
        case 2:
            color = tex2D(facade2, uv);
            break;
        case 3:
            color = tex2D(facade3, uv);
            break;
        case 4:
            color = tex2D(facade4, uv);
            break;
        case 5:
            color = tex2D(facade5, uv);
            break;
        case 6:
            color = tex2D(facade6, uv);
            break;
        case 7:
            color = tex2D(facade7, uv);
            break;      
    }
}

#endif