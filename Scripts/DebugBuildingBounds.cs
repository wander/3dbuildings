using UnityEngine;

#if UNITY_EDITOR

namespace Wander
{
    public class DebugBuildingBounds : MonoBehaviour
    {
        public Color color = Color.red;
        public Bounds bounds;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = color;
            Gizmos.DrawWireCube( bounds.center, bounds.size );   
        }
    }
}

#endif