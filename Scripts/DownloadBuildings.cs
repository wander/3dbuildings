using B3dm.Tile;
using Newtonsoft.Json.Linq;
using SharpGLTF.Memory;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using static UnityEngine.Mesh;

#if UNITY_EDITOR

namespace Wander
{
    [System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Sequential )]
    internal struct GLBVertex // Must be multiple of 4.
    {
        internal Vector3 pos;
        internal Vector3 normal;
        internal Color32 color;
        internal Vector2 uv;
        internal float batchId;
    }

    struct GLbTempData
    {
        public IList<uint>  indices;
        public Vector3Array vertices;
        public Vector3Array normals;
        public ScalarArray  batchIds;
    }

    enum TileBuildState
    {
        DownloadB3dm,
        LoadGLB,
        BuildMesh,
        Finished
    }

    class AerialData
    {
        internal double rdx, rdy;
        internal bool started;
        internal UnityWebRequest www;
        internal Color32[] data;
        internal int width;
        internal int height;
        internal bool retried;
    }

    /* This only works currently for RD coordinates. */
    class BuildingTile
    {
        public TileBuildState state;
        public UnityWebRequest www;
        public Task task;
        public float timeSinceWanted;
        public List<GLbTempData> glbData;
        public Mesh.MeshDataArray meshArrays;
        public Bounds bounds;
        public Dictionary<(int bId, int typ), (int cnt, int ofs)> offsetMapNew = new();
    }

    [ExecuteAlways()]
    public class DownloadBuildings : MonoBehaviour
    {
        public List<HeightData> heightDatas;

        [ReadOnly] public double tileSize;
        [ReadOnly] public Bounds bounds = new Bounds(Vector3.zero, new Vector3(1000, 100, 1000));
        public double rdOffsetX = 174041;
        public double rdOffsetY = 444126;
        public float limitSize = 2000;
        public bool autoDownloadHeight = true;
        public bool colorRoofsWithAerialData = true;
        public bool skipGeometryOutsideTerrain = true;
        public bool saveToDisk = true;
        public bool showInEditor = true;
        public bool swapIndices02 = false;
        public bool splitBatches = false;
        public float flatRoofY = 0.95f;
        public float roofY = 0.3f;
        public int heightMapRes = 256;
        public int aerialMapResPer100Mtr = 256;
        public int aerialMapMaxRes = 2048;
        public string tilesetUrl = "https://3dbag.nl/download/3dtiles/v210908_fd2cee53/lod22/tileset.json";
        public int maxSimultaneousDownloads = 8;
        public int maxSimultaneousAsyncBuilds = 2;
        public int normalResolution = 8;
        public List<Material> walls;
        public List<Material> roofs;
        public List<Material> flatRoofs;
        public List<Material> aerialColoredRoofs;

        [Header("Debug")]
        public bool showNumProgressAndFinished;
        public bool showTimings;
        public bool addScriptToShowTileBounds;

        GameObject root;
        JToken jsonRoot;
        Vector3 transformOffset;
        Dictionary<string, BuildingTile> requests;
        Dictionary<JToken, Bounds> boundsLookup;
        List<string> pendingRemoves;
        List<AsyncHeightData> asyncHeightHandles;
        List<AerialData> aerialHandles;

        string tilesUrl;

        int numFinishedDownloads;
        int numFinishedGlbLoads;
        int numFinishedMeshBuilds;

        // For debug drawning th rounded area when converting bounding box to tiles to obtain height and aerial data.
        Vector2Int minTile;
        Vector2Int maxTile;

        public void SetGPSCoord( Dictionary<string, string> boundaryData )
        {
            var wgs84Lat = double.Parse( boundaryData["gpsx"] );
            var wgs84Lon = double.Parse( boundaryData["gpsy"] );
            tileSize = float.Parse( boundaryData["tileSize"] );
            RDUtils.GPS2RD( wgs84Lat, wgs84Lon, out rdOffsetX, out rdOffsetY );
            var boundsSize = float.Parse( boundaryData["boundsSize"] );
            var offsX = float.Parse( boundaryData["offsetx"] );
            var offsY = float.Parse( boundaryData["offsety"] );
            bounds = new Bounds( new Vector3( offsX, 0, offsY ) + new Vector3( (float)tileSize, 0, (float)tileSize )/2, new Vector3( boundsSize, 100, boundsSize ) );
            if (limitSize > 0)
            {
                bounds.extents = new Vector3( limitSize/2, bounds.extents.y, limitSize/2 );
                UnityEngine.Debug.LogWarning( "Bounds size is limited by limitSize in inspector. Put on 0 or less to obtain bounds from terrain." );
            }

#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        public string GetAerialUrl( double minX, double minY, double maxX, double maxY, int res )
        {
            if (res > aerialMapMaxRes)
            {
                res = aerialMapMaxRes;
            }

            // Do not use TMS as that contains the resolution for us, which is a little too low and cannot be adjusted.
            string url = $"https://service.pdok.nl/hwh/luchtfotorgb/wms/v1_0?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image" +
                $"%2Fpng&TRANSPARENT=true&layers=Actueel_orthoHR&CRS=EPSG%3A28992&STYLES=&WIDTH={res}&HEIGHT={res}&BBOX={minX}" +
                $"%2C{minY}%2C{maxX}%2C{maxY}";

            return url;
        }

        public void Generate()
        {
            Cancel();
            tilesUrl = tilesetUrl.Replace( "/tileset.json", "" );
            root = new GameObject( "Buildings" );
            root.AddComponent<BuildingBlockEnabler>();

            if (autoDownloadHeight)
                asyncHeightHandles = new List<AsyncHeightData>();

            if (colorRoofsWithAerialData)
                aerialHandles = new List<AerialData>();

            // For height data and aerial, we retreive the data on a per tile basis, because covering the whole area might
            // not be possible at once.
            var rdMinX = rdOffsetX+bounds.center.x-bounds.size.x/2;
            var rdMaxX = rdMinX + bounds.size.x;
            var rdMinY = rdOffsetY+bounds.center.z-bounds.size.z/2;
            var rdMaxY = rdMinY + bounds.size.z;

            // Split up into tiles of zoom 12 (215 meter each).
            int zoom = RDUtils.GetZoomFromTileSize( (float)tileSize );
            minTile  = RDUtils.RD2Tile( rdMinX, rdMinY, zoom );
            maxTile  = RDUtils.RD2Tile( rdMaxX, rdMaxY, zoom );
            var nTiles       = maxTile-minTile;
            nTiles.y *= -1;
            //nTiles += new Vector2Int( 1, 1 );

            int arealMapRes = Mathf.RoundToInt( (float)(tileSize/100) * aerialMapResPer100Mtr );

            for (int y = 0;y <= nTiles.y;y++)
            {
                for (int x = 0;x <= nTiles.x;x++)
                {
                    var tileAddr = minTile + new Vector2Int( x, -y );
                    var tileRD = RDUtils.Tile2RD(tileAddr, zoom);

                    if (autoDownloadHeight)
                    {
                        var heightUrl = GeoTiffHeight.BuildPDOKWCSUrl( tileRD.rdX, tileRD.rdX+tileSize, tileRD.rdY, tileRD.rdY+tileSize, heightMapRes, heightMapRes);
                        asyncHeightHandles.Add( GeoTiffHeight.LoadFromUrl( heightUrl, true, 10, 0, false ) );
                    }

                    if (colorRoofsWithAerialData)
                    {
                        var aerialUrl = GetAerialUrl( tileRD.rdX, tileRD.rdY, tileRD.rdX+tileSize, tileRD.rdY+tileSize, arealMapRes );
                        var ad = new AerialData();
                        ad.www = UnityWebRequestTexture.GetTexture( aerialUrl );
                        ad.rdx = tileRD.rdX;
                        ad.rdy = tileRD.rdY;
                        aerialHandles.Add( ad );
                    }
                }
            }

            if (asyncHeightHandles != null)
            {
                // Start only x simultaneously
                for (int i = 0;i <Mathf.Min( asyncHeightHandles.Count, maxSimultaneousDownloads );i++)
                {
                    asyncHeightHandles[i].Start();
                }
            }

            if (aerialHandles != null)
            {
                // Start only x simultaneously
                for (int i = 0;i < Mathf.Min( maxSimultaneousDownloads, aerialHandles.Count );i++)
                {
                    aerialHandles[i].www.SendWebRequest();
                    aerialHandles[i].started=true;
                }
            }

            StartCoroutine( DownloadTileset() );
            StartCoroutine( ProcessTiles() );
        }

        internal void Cancel()
        {
            StopAllCoroutines();
            Clean();
            numFinishedDownloads    = 0;
            numFinishedGlbLoads     = 0;
            numFinishedMeshBuilds   = 0;
            heightDatas = null;
            asyncHeightHandles = null;
            aerialHandles = null;
        }

        void Clean()
        {
            root = null;
            jsonRoot = null;
            requests = new Dictionary<string, BuildingTile>();
            boundsLookup = new Dictionary<JToken, Bounds>();
            pendingRemoves = new List<string>();
        }

        /* ------- Helper functions ---------------------------------------------------------------------------------------------------------------------- */

        public static Vector3 ReduceResolution( Vector3 v, float resolution = 32 )
        {
            UnityEngine.Debug.Assert( Mathf.Abs( Vector3.Dot( v, v )-1 )<0.001f ); // Assert normalized.
            float invRes = 1.0f / resolution;
            var n = v * resolution;
            n.x = Mathf.Round( n.x );
            n.y = Mathf.Round( n.y );
            n.z = Mathf.Round( n.z );
            return n*invRes;
        }

        public static Vector3 CrossWithUpVector( Vector3 v )
        {
            return new Vector3( -v.z, 0, v.x );
        }

        public static void GenerateUvs( Vector3 n, Vector3 v0, Vector3 v1, Vector3 v2, out Vector2 uv0, out Vector2 uv1, out Vector2 uv2 )
        {
            Vector3 u = CrossWithUpVector( n );

            if (Vector3.Dot( u, u ) < 0.01f)
                u = Vector3.right;
            else
                u = u.normalized;

            Vector3 v = (Vector3.Cross(n, u)).normalized;

            uv0 = new Vector2( Vector3.Dot( v0, u ), Vector3.Dot( v0, v ) );
            uv1 = new Vector2( Vector3.Dot( v1, u ), Vector3.Dot( v1, v ) );
            uv2 = new Vector2( Vector3.Dot( v2, u ), Vector3.Dot( v2, v ) );
        }

        bool IsDownloadErrror( UnityWebRequest request )
        {
            return !(request.result == UnityWebRequest.Result.Success || request.result == UnityWebRequest.Result.InProgress);
        }

        Bounds ToUnityBounds( JToken volume )
        {
            if (!boundsLookup.TryGetValue( volume, out Bounds bb ))
            {
                var floatArray = volume["box"].Values().ToArray();
                bb.center  = new Vector3( (float)floatArray[0], (float)floatArray[2], (float)floatArray[1] );
                bb.extents = new Vector3( (float)floatArray[3], (float)floatArray[11], (float)floatArray[7] );
                boundsLookup.Add( volume, bb );
            }
            return bb;
        }

        void PrepareTileGLBData( BuildingTile bt, SharpGLTF.Schema2.ModelRoot modelRoot )
        {
            System.Diagnostics.Debug.Assert( bt.glbData==null );
            bt.glbData = new List<GLbTempData>();
            foreach (var mesh in modelRoot.LogicalMeshes)
            {
                foreach (var prim in mesh.Primitives)
                {
                    if (prim.DrawPrimitiveType != SharpGLTF.Schema2.PrimitiveType.TRIANGLES)
                    {
                        UnityEngine.Debug.LogError( "Triangles are only supported." );
                        continue;
                    }

                    var idxAccessor  = prim.GetIndexAccessor();
                    var posAccessor  = prim.GetVertexAccessor( "POSITION" );
                    var norAccessor  = prim.GetVertexAccessor( "NORMAL" );
                    var bachAccessor = prim.GetVertexAccessor( "_BATCHID" );

                    if (posAccessor.Dimensions != SharpGLTF.Schema2.DimensionType.VEC3)
                    {
                        UnityEngine.Debug.LogWarning( "Only support vec3 position type." );
                        continue;
                    }
                    if (norAccessor.Dimensions != SharpGLTF.Schema2.DimensionType.VEC3)
                    {
                        UnityEngine.Debug.LogWarning( "Only support vec3 position type." );
                        continue;
                    }
                    if (prim.GetVertexAccessor( "NORMAL" ).Dimensions != SharpGLTF.Schema2.DimensionType.VEC3)
                    {
                        UnityEngine.Debug.LogWarning( "Only support vec3 position type." );
                        continue;
                    }
                    if (idxAccessor.Dimensions != SharpGLTF.Schema2.DimensionType.SCALAR ||
                        !(idxAccessor.Format.ByteSize == 2 || prim.GetIndexAccessor().Format.ByteSize == 4))
                    {
                        UnityEngine.Debug.LogWarning( "Index type must be scalar and either a 2 or 4 bytes." );
                        continue;
                    }
                    if (bachAccessor.Dimensions != SharpGLTF.Schema2.DimensionType.SCALAR)
                    {
                        UnityEngine.Debug.LogWarning( "BatchID not valid type." );
                        continue;
                    }

                    GLbTempData glbData = new GLbTempData();
                    glbData.indices  = prim.GetIndices();
                    glbData.vertices = prim.GetVertices( "POSITION" ).AsVector3Array();
                    glbData.normals  = prim.GetVertices( "NORMAL" ).AsVector3Array();
                    glbData.batchIds = prim.GetVertices( "_BATCHID" ).AsScalarArray();
                    bt.glbData.Add( glbData );
                }
            }
        }

        /*  Download tileset.json and use the file with its bounding boxes (quadtree) to determine which tiles need to be downloaded. */
        IEnumerator DownloadTileset()
        {
            int numRetries = 3;
            while (numRetries-- >= 0) // This sometimes fails.. give couple retries. TODO find out why it sometimes fails? Maybe HTTP (engine) is not fully initialised on Start().
            {
                var www = UnityWebRequest.Get( tilesetUrl );
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.Success)
                {
                    var data = www.downloadHandler.data;
                    yield return Task.Run( () =>
                    {
                        // assume is utf8 encoding.
                        var json  = JObject.Parse( Encoding.UTF8.GetString(data) );
                        var jRoot = json["root"];
                        var floatArray = jRoot["transform"].Values().ToArray();
                        transformOffset = new Vector3( (float)floatArray[12], (float)floatArray[14], (float)floatArray[13] );
                        jsonRoot = jRoot; // Set at last, as it is used as a waiting handle to have DownloadTiles start.
                    } );
                    yield break; // Done.
                }
                else
                {
                    UnityEngine.Debug.LogError( www.error );
                }
            }
        }

        IEnumerator AwaitHeightData()
        {
            // Await height data.
            if (autoDownloadHeight && asyncHeightHandles != null)
            {
                int prevFinished = 0;
                while (true)
                {
                    int numStarted  = 0;
                    int numFinished = 0;

                    for (int i = 0;i <asyncHeightHandles.Count;i++)
                    {
                        if (!asyncHeightHandles[i].IsFinished())
                        {
                            if (!asyncHeightHandles[i].Started && numStarted<maxSimultaneousDownloads)
                            {
                                asyncHeightHandles[i].Start();
                                numStarted++;
                            }
                        }
                        else if (!asyncHeightHandles[i].Valid && !asyncHeightHandles[i].Retried)
                        {
                            asyncHeightHandles[i].Retry();
                            numStarted++;
                        }
                        else
                        {
                            numFinished++;
                        }
                    }

                    if (numFinished != prevFinished)
                    {
                        prevFinished = numFinished;
                        UnityEngine.Debug.Log( "Num heights finished : " + numFinished + "/" +  asyncHeightHandles.Count );
                    }

                    if (numFinished == asyncHeightHandles.Count)
                    {
                        break;
                    }

                    yield return null;
                }

                heightDatas = new List<HeightData>();
                for (int i = 0;i <asyncHeightHandles.Count;i++)
                {
                    heightDatas.Add( asyncHeightHandles[i].HeightData );
                }
            }
        }

        IEnumerator AwaitAerialPictures()
        {
            if (colorRoofsWithAerialData && aerialHandles != null)
            {
                int prevFinished = 0;
                while (true)
                {
                    int numProcessing  = 0;
                    int numFinished = 0;

                    for (int i = 0;i <aerialHandles.Count;i++)
                    {
                        var handler  = (DownloadHandlerTexture)aerialHandles[i].www.downloadHandler;
                        if (aerialHandles[i].www != null && aerialHandles[i].www.result == UnityWebRequest.Result.InProgress)
                        {
                            numProcessing++;
                        }

                        bool isError = false;
                        if (!handler.isDone)
                        {
                            if (!aerialHandles[i].started && numProcessing<maxSimultaneousDownloads)
                            {
                                aerialHandles[i].started=true;
                                aerialHandles[i].www.SendWebRequest();
                                numProcessing++;
                            }
                            var res = aerialHandles[i].www.result;
                            if (res == UnityWebRequest.Result.ProtocolError || res == UnityWebRequest.Result.DataProcessingError || res == UnityWebRequest.Result.ConnectionError)
                            {
                                isError = true;
                            }
                        }

                        if (isError && !aerialHandles[i].retried)
                        {
                            aerialHandles[i].retried = true;
                            aerialHandles[i].www = UnityWebRequestTexture.GetTexture( aerialHandles[i].www.uri );
                            aerialHandles[i].www.SendWebRequest();
                            numProcessing++;
                        }

                        if (aerialHandles[i].www.result != UnityWebRequest.Result.InProgress)
                        {
                            // cache texture, as www cannot be accessed in other thread.
                            if (aerialHandles[i].www.isDone && aerialHandles[i].www.result == UnityWebRequest.Result.Success)
                            {
                                var tex = ((DownloadHandlerTexture)aerialHandles[i].www.downloadHandler).texture;
                                if (aerialHandles[i].data == null && tex != null)
                                {
                                    aerialHandles[i].data   = tex.GetPixels32( 0 );
                                    aerialHandles[i].width  = tex.width;
                                    aerialHandles[i].height = tex.height;
                                }
                            }
                            numFinished++;
                        }
                    }

                    if (numFinished != prevFinished)
                    {
                        prevFinished = numFinished;
                        UnityEngine.Debug.Log( "Num areal datas finished : " + numFinished + "/" +  aerialHandles.Count );
                    }

                    if (numFinished == aerialHandles.Count)
                    {
                        break;
                    }

                    yield return null;
                }
            }
        }

        IEnumerator ProcessTiles()
        {
            yield return AwaitHeightData();
            yield return AwaitAerialPictures();

            // Await tileset to be downloaded.
            if (jsonRoot == null)
            {
                yield return null;
            }

            while (true)
            {
                var unityTime = Time.time; // Can only be called on MainThread.
                Stopwatch sw = new Stopwatch();

                // Read Tiles
                {
                    sw.Restart();
                    yield return Task.Run( () =>
                    {
                        ReadTileset( unityTime );
                    } );
                    if (showTimings) UnityEngine.Debug.Log( "ReadTileSet took: "+ sw.ElapsedMilliseconds + "ms" );
                }

                // Download B3dms
                {
                    sw.Restart();
                    DownloadB3dms();
                    if (showTimings) UnityEngine.Debug.Log( "Download b3dms took: "+ sw.ElapsedMilliseconds + "ms" );
                }

                // Load GLB data
                {
                    sw.Restart();
                    LoadGLBDatas();
                    if (showTimings) UnityEngine.Debug.Log( "Download GLBDatas: "+ sw.ElapsedMilliseconds + "ms" );
                }

                // Load Unity Mesh data
                {
                    sw.Restart();
                    BuildUnityMeshesAndGameObjects();
                    if (showTimings) UnityEngine.Debug.Log( "Build unity meshes: "+ sw.ElapsedMilliseconds + "ms" );
                }

                RemoveFailedOnes();

                yield return new WaitForSeconds( 0.02f );
            }
        }

        void UpdateStats( TileBuildState forState, ref int numFinished, Func<BuildingTile, bool> isValid, Func<BuildingTile, bool> isFinished )
        {
#if UNITY_EDITOR
            if (!showNumProgressAndFinished)
                return;

            int numProgress = 0;
            foreach (var kvp in requests)
            {
                if (!(kvp.Value.state == forState))
                    continue;
                if (isValid( kvp.Value ))
                {
                    if (!isFinished( kvp.Value ))
                        numProgress++;
                    else
                        numFinished++;
                }
            }
            UnityEngine.Debug.Log( forState.ToString() + " | " + numFinished + "/" + (requests.Count) );
#endif
        }

        void RemoveFailedOnes()
        {
            // Get rid of tiles that failed.
            pendingRemoves.ForEach( a => requests.Remove( a ) );
            pendingRemoves.Clear();
        }

        void ReadTileset( float unityTime )
        {
            var stack = new List<JToken>(){jsonRoot};
            do
            {
                var tile = stack[stack.Count-1];
                stack.RemoveAt( stack.Count-1 );
                Bounds tileBounds  = ToUnityBounds( tile["boundingVolume"] );
                tileBounds.center += new Vector3( (float)(transformOffset.x-rdOffsetX), 0, (float)(transformOffset.z-rdOffsetY) );
                if ( tileBounds.Intersects( bounds ))
                {
                    var children = tile["children"];
                    if (children == null) // isLeaf
                    {
                        var name = tile["content"]["uri"].ToString();
                        if (requests.TryGetValue( name, out BuildingTile bt ))
                        {
                            // if already in, update time
                            bt.timeSinceWanted = unityTime;
                        }
                        else
                        {
                            bt = new BuildingTile();
                            bt.timeSinceWanted = unityTime;
                            bt.bounds = tileBounds;
                            bt.state  = TileBuildState.DownloadB3dm;
                            requests.Add( name, bt );
                        }
                    }
                    else
                    {
                        tile = children.First;
                        while (tile != null)
                        {
                            stack.Add( tile );
                            tile = tile.Next;
                        }
                    }
                }
            } while (stack.Count > 0);
        }

        void DownloadB3dms()
        {
            int maxRequestsSimultaneously = maxSimultaneousDownloads;
            foreach (var kvp in requests)
            {
                BuildingTile bt = kvp.Value;
                if (bt.state != TileBuildState.DownloadB3dm)
                    continue;

                if (kvp.Value.www == null)
                {
                    bt.www = UnityWebRequest.Get( tilesUrl + "/" + kvp.Key );
                    bt.www.SendWebRequest();
                    maxRequestsSimultaneously--;
                }
                else if (IsDownloadErrror( bt.www ) || (Time.time - bt.timeSinceWanted > 1)) // If is error, if no longer wanted (due to scrolling over the tile for example -> out of view).
                {
                    // Try to cancel to limit bandwidth usage.
                    bt.www.Abort();
                    pendingRemoves.Add( name );
                    if (IsDownloadErrror( bt.www ))
                    {
                        UnityEngine.Debug.LogError( bt.www.error );
                    }
                    numFinishedDownloads++;
                    UnityEngine.Debug.Log( "Download B3dm | " + numFinishedDownloads + "/" + (requests.Count) );
                }
                else if (bt.www.result == UnityWebRequest.Result.Success)
                {
                    bt.state = TileBuildState.LoadGLB;
                    numFinishedDownloads++;
                    UnityEngine.Debug.Log( "Download B3dm | " + numFinishedDownloads + "/" + (requests.Count) );
                }
                else if (bt.www.result == UnityWebRequest.Result.InProgress)
                {
                    maxRequestsSimultaneously--;
                }

                if (maxRequestsSimultaneously <= 0)
                    break;
            }
        }

        void LoadGLBDatas()
        {
            int maxSimultaneousBuilds = maxSimultaneousAsyncBuilds;
            foreach (var kvp in requests)
            {
                BuildingTile bt = kvp.Value;
                if (bt.state != TileBuildState.LoadGLB)
                    continue;

                if (bt.task == null)
                {
                    maxSimultaneousBuilds--;
                    var data = bt.www.downloadHandler.data;
                    bt.task = Task.Run( () =>
                    {
                        using (var stream = new MemoryStream( data ))
                        {
                            Stopwatch sw = new Stopwatch();
                            // b3dm
                            sw.Restart();
                            var b3dm = B3dmReader.ReadB3dm( stream );
                            if (showTimings) UnityEngine.Debug.Log( "Load b3dm time: " + sw.ElapsedMilliseconds + "ms" );
                            // glb
                            sw.Restart();
                            var model = SharpGLTF.Schema2.ModelRoot.ParseGLB( b3dm.GlbData );
                            if (showTimings) UnityEngine.Debug.Log( "Parse glb time: " + sw.ElapsedMilliseconds + "ms" );
                            // prepare glb data into so can be applied to meshes async
                            sw.Restart();
                            PrepareTileGLBData( bt, model );
                            if (showTimings) UnityEngine.Debug.Log( "Prepare glb time: " + sw.ElapsedMilliseconds + "ms" );
                        }
                    } );
                }
                else if (!bt.task.IsCompleted)
                {
                    maxSimultaneousBuilds--;
                }
                else
                {
                    if (bt.task.IsCompletedSuccessfully)
                    {
                        bt.state = TileBuildState.BuildMesh;
                        bt.task = null;
                    }
                    else
                    {
                        // Failed.
                        pendingRemoves.Add( kvp.Key );
                    }
                    numFinishedGlbLoads++;
                    UnityEngine.Debug.Log( "Load GLB | " + numFinishedGlbLoads + "/" + (requests.Count) );
                }

                // Prevent all cores to be occupied 
                if (maxSimultaneousBuilds <= 0)
                    return;
            }
        }

        bool TryGetHeight( double x, double y, out float height )
        {
            height = 0;
            if (heightDatas == null)
            {
                return false;
            }
            for (int i = 0;i < heightDatas.Count;i++)
            {
                height = heightDatas[i].FromRD( x, y, out bool wasFound );
                if (wasFound)
                {
                    return true;
                }
            }
            return false;
        }

        private Color32 TryGetColorFromAerialData( Vector3 rd, ref int cachedArealIdx )
        {
            Color32 c = Color.black;
            if (aerialHandles==null)
                return c;
            for (int i = 0;i<aerialHandles.Count;i++)
            {
                int idx = (i + cachedArealIdx) % aerialHandles.Count;
                if (aerialHandles[idx].data == null)
                    continue;
                var x = (float)( (rd.x + rdOffsetX) - aerialHandles[idx].rdx );
                var y = (float)( (rd.z + rdOffsetY) - aerialHandles[idx].rdy );
                int texX = Mathf.FloorToInt( (float)(x/tileSize)*aerialHandles[idx].width + 0.5f );
                int texY = Mathf.FloorToInt( (float)(y/tileSize)*aerialHandles[idx].height + 0.5f );
                if (texX >= 0 && texX < aerialHandles[idx].width &&
                    texY >= 0 && texY < aerialHandles[idx].height)
                {
                    //  texY = aerialHandles[i].height - 1 - texY;
                    c = aerialHandles[idx].data[texY * aerialHandles[idx].width + texX];
                    cachedArealIdx = idx;
                    return c;
                }
            }
            return c;
        }

        void BuildUnityMeshesAndGameObjects()
        {
            int maxSimultaneousBuilds = maxSimultaneousAsyncBuilds;
            foreach (var kvp in requests)
            {
                BuildingTile bt = kvp.Value;
                if (!(bt.state == TileBuildState.BuildMesh))
                    continue;

                if (bt.task == null)
                {
                    maxSimultaneousBuilds--;
                    bt.meshArrays = AllocateWritableMeshData( bt.glbData.Count ); // Can only be accessed on MT

                    // Do the part that can be done async
                    bt.task = Task.Run( ()=> 
                    //bt.task = new Task( () =>
                    {
                        try
                        {
                            Stopwatch sw = new Stopwatch();
                            sw.Restart();

                            // Vertex layout same for all glbs
                            var vertexLayout = new[] // Layout must be multiple of 4
                        {
                            new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                            new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.Float32, 3),
                            new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.UNorm8, 4),
                            new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, 3)
                        };

                            for (int j = 0;j < bt.glbData.Count;j++)
                            {
                                var glbData = bt.glbData[j];
                                var vertexCount = glbData.vertices.Count;
                                var indexCount  = glbData.indices.Count;

                                Mesh.MeshData meshData = bt.meshArrays[j];

                                // Vertices, keep dictionary for remapping of indices for geometry that is deliberately skipped (outside tile).
                                var listVertices = new List<GLBVertex>();
                                var remapIndices = new Dictionary<int, uint>();

                                float le = bounds.center.x - bounds.extents.x;
                                float ri = bounds.center.x + bounds.extents.x;
                                float up = bounds.center.z + bounds.extents.z;
                                float dw = bounds.center.z - bounds.extents.z;

                                {
                                    GLBVertex vertex = new GLBVertex();
                                    uint remapCounter = 0;
                                    for (int i = 0;i < vertexCount;i++)
                                    {
                                        double rdx = transformOffset.x + glbData.vertices[i].X;
                                        double rdy = transformOffset.z + (-glbData.vertices[i].Z);
                                        float y    = transformOffset.y + glbData.vertices[i].Y;
                                        if (skipGeometryOutsideTerrain)
                                        {
                                            double wx = rdx-rdOffsetX;
                                            double wy = rdy-rdOffsetY;
                                            if ((wx<le) || (wx>ri) || (wy<dw) || (wy>up))
                                            {
                                                continue;
                                            }
                                        }
                                        if (TryGetHeight( rdx, rdy, out float height ))
                                        {
                                            y = height + glbData.vertices[i].Y;
                                        }
                                        vertex.pos.x = (float)(rdx - rdOffsetX);
                                        vertex.pos.y = y;
                                        vertex.pos.z = (float)(rdy - rdOffsetY);
                                        vertex.normal.x = glbData.normals[i].X;
                                        vertex.normal.y = glbData.normals[i].Y;
                                        vertex.normal.z = -glbData.normals[i].Z;
                                        vertex.batchId  = glbData.batchIds[i];
                                        listVertices.Add( vertex );
                                        remapIndices.Add( i, remapCounter++ );
                                    }
                                }

                                // Swap indices if requested.
                                if (swapIndices02)
                                {
                                    for (int i = 0;i < indexCount;i += 3)
                                    {
                                        var t = glbData.indices[i+0];
                                        glbData.indices[i+0] = glbData.indices[i+2];
                                        glbData.indices[i+2] = t;
                                    }
                                }

                                // Generate Uvs TODO this is not entirely correct, should split vertices, where uv's differ, but good enough for now.
                                var listIndices = new List<uint>();
                                {
                                    // NOTE: Indices might already been swapped.
                                    for (int i = 0;i < indexCount;i += 3)
                                    {
                                        int i0 = (int)glbData.indices[i+0];
                                        int i1 = (int)glbData.indices[i+1];
                                        int i2 = (int)glbData.indices[i+2];

                                        // Original indices may no longer be in use, after being clipped at tile boundary. Look up if they still exist.
                                        if (!(remapIndices.TryGetValue( i0, out uint r0 ) &&
                                               remapIndices.TryGetValue( i1, out uint r1 ) &&
                                               remapIndices.TryGetValue( i2, out uint r2 )))
                                        {
                                            continue;
                                        }

                                        var v0 = listVertices[(int)r0];
                                        var v1 = listVertices[(int)r1];
                                        var v2 = listVertices[(int)r2];

                                        // Attempt to generate UV coordinates.
                                        var n  = ReduceResolution( v0.normal, normalResolution ); /* TODO should be triangle normal, but jut pick one vertex for now*/
                                        GenerateUvs( n, v0.pos, v1.pos, v2.pos, out v0.uv, out v1.uv, out v2.uv );

                                        // Save back change.
                                        listVertices[(int)r0] = v0;
                                        listVertices[(int)r1] = v1;
                                        listVertices[(int)r2] = v2;

                                        listIndices.AddRange( new[] { r0, r1, r2 } );
                                    }
                                }

                                // Now the vertices count (with skipped geometry outside terrain) is known.
                                meshData.SetVertexBufferParams( listVertices.Count, vertexLayout );
                                NativeArray<GLBVertex> nativeVertices = meshData.GetVertexData<GLBVertex>();
                                for (int i = 0;i < listVertices.Count;i++)
                                {
                                    nativeVertices[i] = listVertices[i];
                                }
                                listVertices = null;
                                remapIndices = null;

                                // Build 3 groups, walls, flat roof, normal roof, based on normal.y.
                                {
                                    // First count, need this.
                                    int numFlatRoofs = 0;
                                    int numRoofs = 0;
                                    int numWalls = 0;
                                    Dictionary<(int bId, int typ), (int cnt, int ofs)> offsetMap = new();
                                    for (int i = 0;i < listIndices.Count;i += 3)
                                    {
                                        var v0 = nativeVertices[(int)listIndices[i]];
                                        Vector3 normal = v0.normal;  /* TODO should be triangle normal, but jut pick one vertex for now*/
                                        var batchId = Mathf.FloorToInt( v0.batchId + 0.5f );
                                        if (normal.y > flatRoofY)
                                        {
                                            numFlatRoofs++; // flat roof
                                            if (splitBatches)
                                            {
                                                if (offsetMap.TryGetValue( (batchId, 0), out var tt ))
                                                {
                                                    tt.cnt += 3;
                                                    offsetMap[(batchId, 0)] = tt;
                                                }
                                                else offsetMap[(batchId, 0)] = (3, 0);
                                            }
                                        }
                                        else if (normal.y > roofY)
                                        {
                                            numRoofs++;// roof
                                            if(splitBatches)
                                            {
                                                if (offsetMap.TryGetValue( (batchId, 1), out var tt ))
                                                {
                                                    tt.cnt += 3;
                                                    offsetMap[(batchId, 1)] = tt;
                                                }
                                                else offsetMap[(batchId, 1)] = (3, 1);
                                            }
                                        }
                                        else
                                        {
                                            numWalls++; // wall
                                            if (splitBatches)
                                            {
                                                if (offsetMap.TryGetValue( (batchId, 2), out var tt ))
                                                {
                                                    tt.cnt += 3;
                                                    offsetMap[(batchId, 2)] = tt;
                                                }
                                                else offsetMap[(batchId, 2)] = (3, 2);
                                            }
                                        }
                                    }
                                    if (splitBatches)
                                    {
                                        int offset = 0;
                                        foreach (var kvp in offsetMap)
                                        {
                                            bt.offsetMapNew.Add( kvp.Key, (0, offset) );
                                            offset += kvp.Value.cnt;
                                        }
                                    }
                                    offsetMap = null;

                                    // Now get index array and supply different groups.
                                    int flatRoofsOfs = 0;
                                    int roofsOfs = numFlatRoofs*3;
                                    int wallsOfs = (numFlatRoofs + numRoofs)*3;
                                    meshData.SetIndexBufferParams( listIndices.Count, IndexFormat.UInt32 );
                                    NativeArray<uint> nativeIndices = meshData.GetIndexData<uint>();
                                    for (int i = 0;i < listIndices.Count;i += 3)
                                    {
                                        var i0  = listIndices[i+0];
                                        var i1  = listIndices[i+1];
                                        var i2  = listIndices[i+2];
                                        var v0  = nativeVertices[(int)i0];
                                        var v1  = nativeVertices[(int)i1];
                                        var v2  = nativeVertices[(int)i2];
                                        var bid = Mathf.FloorToInt( v0.batchId + 0.5f );
                                        Vector3 normal = v0.normal;  /* TODO should be triangle normal, but jut pick one vertex for now*/
                                        if (normal.y > flatRoofY) // flat roof
                                        {
                                            if (splitBatches)
                                            {
                                                var index = bt.offsetMapNew[(bid, 0)];
                                                nativeIndices[index.ofs + index.cnt + 0] = i0;
                                                nativeIndices[index.ofs + index.cnt + 1] = i1;
                                                nativeIndices[index.ofs + index.cnt + 2] = i2;
                                                bt.offsetMapNew[(bid, 0)] = (index.cnt + 3, index.ofs);
                                            }
                                            else
                                            {
                                                nativeIndices[flatRoofsOfs++] = i0;
                                                nativeIndices[flatRoofsOfs++] = i1;
                                                nativeIndices[flatRoofsOfs++] = i2;
                                            }
                                        }
                                        else if (normal.y > roofY) // roof
                                        {
                                            if (splitBatches)
                                            {
                                                var index = bt.offsetMapNew[(bid, 1)];
                                                nativeIndices[index.ofs + index.cnt + 0] = i0;
                                                nativeIndices[index.ofs + index.cnt + 1] = i1;
                                                nativeIndices[index.ofs + index.cnt + 2] = i2;
                                                bt.offsetMapNew[(bid, 1)] = (index.cnt + 3, index.ofs);
                                            }
                                            else
                                            {
                                                nativeIndices[roofsOfs++] = i0;
                                                nativeIndices[roofsOfs++] = i1;
                                                nativeIndices[roofsOfs++] = i2;
                                            }
                                        }
                                        else // wall
                                        {
                                            if (splitBatches)
                                            {
                                                var index = bt.offsetMapNew[(bid, 2)];
                                                nativeIndices[index.ofs + index.cnt + 0] = i0;
                                                nativeIndices[index.ofs + index.cnt + 1] = i1;
                                                nativeIndices[index.ofs + index.cnt + 2] = i2;
                                                bt.offsetMapNew[(bid, 2)] = (index.cnt + 3, index.ofs);
                                            }
                                            else
                                            {
                                                nativeIndices[wallsOfs++] = i0;
                                                nativeIndices[wallsOfs++] = i1;
                                                nativeIndices[wallsOfs++] = i2;
                                            }
                                        }
                                    }
                                    //if (!splitBatches)
                                    {
                                        System.Diagnostics.Debug.Assert( flatRoofsOfs/3 == numFlatRoofs );
                                        System.Diagnostics.Debug.Assert( roofsOfs/3-numFlatRoofs == numRoofs );
                                        System.Diagnostics.Debug.Assert( wallsOfs/3-(numFlatRoofs+numRoofs) == numWalls );
                                    }

                                    // Set vertex colors from aerial data.
                                    if (colorRoofsWithAerialData)
                                    {
                                        int cachedArealIdx  = 0;
                                        var colorPerBatchId = new Dictionary<int, (Color col, int num)>();
                                        for (int i = 0;i < numFlatRoofs + numRoofs;i++)
                                        {
                                            int k  = i*3;
                                            var i0 = nativeIndices[k+0];
                                            var i1 = nativeIndices[k+1];
                                            var i2 = nativeIndices[k+2];
                                            var v0 = nativeVertices[(int)i0];
                                            var v1 = nativeVertices[(int)i1];
                                            var v2 = nativeVertices[(int)i2];
                                            int batchId = Mathf.FloorToInt( v0.batchId + 0.5f );
                                            var c = TryGetColorFromAerialData( (v0.pos+v1.pos+v2.pos)*0.33333f, ref cachedArealIdx );
                                            if (!(colorPerBatchId.TryGetValue( batchId, out var roofColor )))
                                            {
                                                colorPerBatchId.Add( batchId, (c, 1) );
                                            }
                                            else
                                            {
                                                roofColor.col += c;
                                                roofColor.num++;
                                                colorPerBatchId[batchId] = roofColor;
                                            }
                                        }
                                        // Now, average the roof colors, and assign them to the vertices.
                                        int cachedBatchId = -1;
                                        Color cachedRoofColor = Color.black;
                                        for (int i = 0;i < nativeVertices.Length;i++)
                                        {
                                            var v0 = nativeVertices[i];
                                            int batchId = Mathf.FloorToInt( v0.batchId + 0.5f );
                                            if (cachedBatchId != batchId && colorPerBatchId.TryGetValue( batchId, out var newRoofColor ))
                                            {
                                                if (newRoofColor.num != 1) // If not averaged yet, do it now.
                                                {
                                                    newRoofColor.col /= newRoofColor.num;
                                                    newRoofColor.num = 1;
                                                    colorPerBatchId[batchId] = newRoofColor; // Store back.
                                                }
                                                cachedRoofColor = newRoofColor.col;
                                                cachedBatchId = batchId;
                                            }
                                            v0.color = cachedRoofColor;
                                            nativeVertices[i] = v0;
                                        }
                                    }

                                    // Specify the groups
                                    if (!splitBatches)
                                    {
                                        meshData.subMeshCount = 3;
                                        meshData.SetSubMesh( 0, new SubMeshDescriptor( 0, numFlatRoofs*3, MeshTopology.Triangles ) );
                                        meshData.SetSubMesh( 1, new SubMeshDescriptor( numFlatRoofs*3, numRoofs*3, MeshTopology.Triangles ) );
                                        meshData.SetSubMesh( 2, new SubMeshDescriptor( (numFlatRoofs+numRoofs)*3, numWalls*3, MeshTopology.Triangles ) );
                                    }
                                    else
                                    {
                                        int k = 0;
                                        meshData.subMeshCount = bt.offsetMapNew.Count;
                                        foreach (var kvp in bt.offsetMapNew)
                                        {
                                            int cnt = kvp.Value.cnt;
                                            int ofs = kvp.Value.ofs;
                                            meshData.SetSubMesh( k++, new SubMeshDescriptor( ofs, cnt, MeshTopology.Triangles ) );
                                        }
                                    }
                                }
                            }

                            if (showTimings) UnityEngine.Debug.Log( "Convert glb verts to unity: " + sw.ElapsedMilliseconds + "ms" );
                        }
                        catch (Exception e)
                        {
                            UnityEngine.Debug.LogException( e );
                        }
                    } );
                    //bt.task.RunSynchronously();
                }
                else if (!bt.task.IsCompleted)
                {
                    maxSimultaneousBuilds--;
                }
                else
                {
                    if (bt.task.IsCompletedSuccessfully)
                    {
                        Stopwatch sw = new Stopwatch();
                        sw.Restart();
                        bt.task = null;
                        // Finish mesh part
                        Mesh mesh = new Mesh();
                        var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontValidateIndices;
                        ApplyAndDisposeWritableMeshData( bt.meshArrays, mesh, flags ); // Cannot be executed on other thread.
                        mesh.RecalculateBounds();
                        // Build GameObject
                        GameObject goTile = new GameObject(kvp.Key);
                        goTile.transform.SetParent( root.transform );
                        var mr = goTile.AddComponent<MeshRenderer>();
                        if ( addScriptToShowTileBounds )
                        {
                            goTile.AddComponent<DebugBuildingBounds>().bounds = bt.bounds;
                        }
                        var flatRoofMat  = flatRoofs != null ? flatRoofs[UnityEngine.Random.Range( 0, flatRoofs.Count )] : null;
                        var roofMat      = roofs != null ? roofs[UnityEngine.Random.Range( 0, roofs.Count )] : null;
                        var wallMat      = walls != null ? walls[UnityEngine.Random.Range( 0, walls.Count )] : null;
                        // Replace roof materials with aerial colored variant if requested for.
                        if (colorRoofsWithAerialData)
                        {
                            if (aerialColoredRoofs == null || aerialColoredRoofs.Count == 0)
                            {
                                UnityEngine.Debug.LogError( "Must have aerialColoredRoof textures if coloring with aerial data." );
                                return;
                            }
                            roofMat = aerialColoredRoofs[UnityEngine.Random.Range( 0, aerialColoredRoofs.Count )];
                            flatRoofMat = aerialColoredRoofs[UnityEngine.Random.Range( 0, aerialColoredRoofs.Count )];
                        }
                        if (!splitBatches)
                        {
                            Material [] mats = new [] { flatRoofMat, roofMat, wallMat };
                            mr.sharedMaterials = mats;
                        }
                        else
                        {
                            int k = 0;
                            var mats = new Material[bt.offsetMapNew.Count];
                            foreach (var kvp2 in bt.offsetMapNew)
                            {
                                var type = kvp2.Key.typ;
                                mats[k++] = (type == 0 ? flatRoofMat : (type == 1 ? roofMat : wallMat));
                            }
                            mr.sharedMaterials = mats;
                        }
                        goTile.AddComponent<MeshFilter>().sharedMesh = mesh;
                        // Done, yee!
#if UNITY_EDITOR
                        if (saveToDisk)
                        {
                            mesh = SaveToDisk( goTile, mesh, kvp.Key.Replace( "tiles/", "" ).Replace( ".", "_" ) );
                        }
#endif
                        if (!showInEditor)
                        {
                            goTile.Destroy();
                        }

                        bt.state = TileBuildState.Finished;
                        if (showTimings)
                        {
                            UnityEngine.Debug.Log( "Finish mesh time: " + sw.ElapsedMilliseconds + "ms" );
                        }
                    }
                    else
                    {
                        // Failed.
                        bt.meshArrays.Dispose();
                        pendingRemoves.Add( kvp.Key );
                    }

                    numFinishedMeshBuilds++;
                    UnityEngine.Debug.Log( "BuildMesh | " + numFinishedMeshBuilds + "/" + (requests.Count) );
                }

                // Prevent all cores to be occupied 
                if (maxSimultaneousBuilds <= 0)
                    return;
            }
        }


#if UNITY_EDITOR
        Mesh SaveToDisk( GameObject goTile, Mesh m, string name )
        {
            string meshName = name + ".mesh";
            string tileName = name + ".prefab";
            string path = Path.Combine( Application.dataPath, SceneManager.GetActiveScene().name, "buildings" );
            if (!Directory.Exists( path ))
            {
                Directory.CreateDirectory( path );
            }
            try
            {
                AssetDatabase.CreateAsset( m, Path.Combine( path, meshName ).ToAssetsPath( true ).FS() );
                PrefabUtility.SaveAsPrefabAsset( goTile, Path.Combine( path, tileName ).ToAssetsPath( true ).FS(), out _ );
            }
            catch (Exception e) { UnityEngine.Debug.LogException( e ); }
            return m;
        }
#endif

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube( bounds.center, bounds.size );
            
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( DownloadBuildings ) )]
    [InitializeOnLoad]
    public class DownloadBuildingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DownloadBuildings buildings = (DownloadBuildings)target;

            if ( buildings.splitBatches )
            {
                EditorGUILayout.HelpBox( "Split batches is turned on, this makes rendering very slow.", MessageType.Warning );
            }
            EditorGUILayout.HelpBox( "Save after generation, otherwise buildings may not appear.", MessageType.Warning );
            if (!buildings.showInEditor)
            {
                EditorGUILayout.HelpBox( "Builddings are destroyed upon construction. ShowInEditor=off.", MessageType.Warning );

            }

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Generate" ))
                {
                    buildings.Generate();
                }

                if (GUILayout.Button( "Preload height" ))
                {
                    string folder = EditorUtility.OpenFolderPanel( "Select folder", "", "" );
                    if (!string.IsNullOrWhiteSpace( folder ))
                    {
                        List<string> paths = new List<string>();
                        paths.AddRange( Directory.GetFiles( folder, "*.tif", SearchOption.AllDirectories ) );
                        paths.AddRange( Directory.GetFiles( folder, "*.tiff", SearchOption.AllDirectories ) );
                        buildings.heightDatas = new List<HeightData>();
                        foreach (var path in paths)
                        {
                            var heightData = GeoTiffHeight.Load( path );
                            buildings.heightDatas.Add( heightData );
                        }
                    }
                    buildings.Cancel();
                }

                if (GUILayout.Button( "Cancel" ))
                {
                    buildings.Cancel();
                }
            }
            GUILayout.EndHorizontal();


            DrawDefaultInspector();
        }
    }

#endif
}

#endif