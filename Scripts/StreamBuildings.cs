﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;

#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif

namespace Wander
{
    [ExecuteInEditMode] // Is needed to sync GPS coords through SendMessage in Edit mode.
    public class StreamBuildings : MonoBehaviour
    {
        public class BuildingTileRequest
        {
            public bool loadFailed;
            public float time;
            public GameObject tile;
            public bool isAsyncProcessRunning;
            public bool isResourceLoadStarted;
            public AsyncOperationHandle<GameObject> resource;
        }

        [ReadOnly] public int zoom;
        [ReadOnly] public double rdOffsetX;
        [ReadOnly] public double rdOffsetY;
        [ReadOnly] public string areaName;
        [ReadOnly] public Vector3 transformOffset;
        [ReadOnly] public GameObject tileRoot;

        public Camera targetCam;
        public string tilesetUrl = "https://data.3dbag.nl/v20240228/3dtiles/lod22/tileset.json";
        public Bounds bounds = new Bounds(Vector3.zero, new Vector3(1000, 100, 1000));
        public float removeDelay = 7;

        private bool loadingEnabled = true;
        private JToken jsonRoot;
        private Dictionary<JToken, Bounds> boundsLookup = new();
        private Dictionary<string, BuildingTileRequest> tileRequests = new();

        public void SetGPSCoord( Dictionary<string, string> boundaryData )
        {
            var wgs84Lat = double.Parse( boundaryData["gpsx"] );
            var wgs84Lon = double.Parse( boundaryData["gpsy"] );
            var boundsSize = float.Parse( boundaryData["boundsSize"] );
            var offsX = float.Parse( boundaryData["offsetx"] );
            var offsY = float.Parse( boundaryData["offsety"] );
            var tileSize = float.Parse( boundaryData["tileSize"] );
            RDUtils.GPS2RD( wgs84Lat, wgs84Lon, out rdOffsetX, out rdOffsetY );
            areaName = boundaryData["areaName"];
            zoom     = int.Parse( boundaryData["zoom"] );
            bounds   = new Bounds( new Vector3( offsX, 0, offsY ) + new Vector3( (float)tileSize, 0, (float)tileSize )/2, new Vector3( boundsSize, 100, boundsSize ) );

#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        private IEnumerator Start()
        {
            if (!Application.isPlaying)
                yield break;

            if (targetCam == null )
            {
                targetCam = Camera.main;
            }
            tileRoot = new GameObject( "Buildings" );
            yield return DownloadTileset();
            while (true)
            {
                ReadTileset( Time.time );
                yield return new WaitForSeconds( 1 );
                CleanupOutdatedTiles();
                yield return new WaitForSeconds( 1 );
            }
        }

        internal IEnumerator BuildInEditor()
        {
            tileRoot = new GameObject( "BuildingsEditor" );
            yield return DownloadTileset();
            ReadTileset( Time.time );
        }

        private void Update()
        {
            if (Input.GetKeyDown( KeyCode.Alpha2 ))
            {
                loadingEnabled = !loadingEnabled;
            }
        }

        private void CleanupOutdatedTiles()
        {
            var removeList = new List<string>();
            float time = Time.time;
            foreach (var kvp in tileRequests)
            {
                var tileRequest = kvp.Value;
                if (tileRequest.time-Time.time < -removeDelay && (!tileRequest.isAsyncProcessRunning || tileRequest.loadFailed))
                {
                    removeList.Add( kvp.Key );
                }
            }
            removeList.ForEach( coord =>
            {
                if (tileRequests.TryGetValue( coord, out var tileRequest ))
                {
                    CleanRequest( tileRequest );
                    tileRequests.Remove( coord );
                }
            } );
        }

        IEnumerator DownloadTileset()
        {
            int numRetries = 3;
            while (numRetries-- >= 0 && jsonRoot == null) // This sometimes fails.. give couple retries. TODO find out why it sometimes fails? Maybe HTTP (engine) is not fully initialised on Start().
            {
                var www = UnityWebRequest.Get( tilesetUrl );
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.Success)
                {
                    var data = www.downloadHandler.data;
                    yield return Task.Run( () =>
                    {
                        // assume is utf8 encoding.
                        var json  = JObject.Parse( Encoding.UTF8.GetString(data) );
                        var jRoot = json["root"];
                        var floatArray = jRoot["transform"].Values().ToArray();
                        transformOffset = new Vector3( (float)floatArray[12], (float)floatArray[14], (float)floatArray[13] );
                        jsonRoot = jRoot; // Set at last, as it is used as a waiting handle to have DownloadTiles start.
                    } );
                }
                else
                {
                    UnityEngine.Debug.LogError( www.error );
                }
            }
        }

        Bounds ToUnityBounds( JToken volume )
        {
            if (!boundsLookup.TryGetValue( volume, out Bounds bb ))
            {
                var floatArray = volume["box"].Values().ToArray();
                bb.center  = new Vector3( (float)floatArray[0], (float)floatArray[2], (float)floatArray[1] );
                bb.extents = new Vector3( (float)floatArray[3], (float)floatArray[11], (float)floatArray[7] );
            }
            return bb;
        }

        void ReadTileset( float unityTime )
        {
            if (jsonRoot == null)
                return;

            if (!loadingEnabled)
                return;

            Bounds invLocalBounds = bounds;
            invLocalBounds.center += new Vector3( (float)(-transformOffset.x+rdOffsetX), 0, (float)(-transformOffset.z+rdOffsetY) );

            if (Application.isPlaying)
            {
                invLocalBounds.center += new Vector3( targetCam.transform.position.x, 0, targetCam.transform.position.z );
            }

            var stack = new List<JToken>(){jsonRoot};
            do
            {
                var tile = stack[stack.Count-1];
                stack.RemoveAt( stack.Count-1 ); // O(1)

                Bounds tileBounds = ToUnityBounds( tile["boundingVolume"] );
                if (tileBounds.Intersects( invLocalBounds )) /* Removed transformOffset, aso also remove transformOffset from bounds, see above. */
                {
                    var children = tile["children"];
                    if (children == null) // isLeaf
                    {
                        var name_ = tile["content"]["uri"].ToString();
                        if (!tileRequests.TryGetValue( name_, out var tileRequest ))
                        {
                            var path = Path.Combine( "Assets", areaName, "buildings", name_.Replace( "tiles/", "" ).Replace( ".", "_" ) + ".prefab" ).FS();
                            tileRequest = new() { time=unityTime, isAsyncProcessRunning=true };
                            tileRequests.Add( name_, tileRequest );
                            Addressables.LoadResourceLocationsAsync( path, typeof( GameObject ) ).Completed += ( location ) =>
                            {
                                if (!location.IsValid() || location.Result.Count <= 0) // This actually happens: valid, yet it is empty.
                                {
                                    tileRequest.loadFailed = true;
                                    tileRequest.isAsyncProcessRunning = false;
                                    return;
                                }

                                tileRequest.resource = Addressables.LoadAssetAsync<GameObject>( location.Result[0] );
                                tileRequest.isResourceLoadStarted = true;
                                tileRequest.resource.Completed += ( resource ) =>
                                {
                                    if (!resource.IsValid())
                                    {
                                        tileRequest.loadFailed = true;
                                        tileRequest.isAsyncProcessRunning = false;
                                        return;
                                    }

                                    InstantiateAsync( resource.Result ).completed += ( asyncOp ) =>
                                    {
                                        tileRequest.isAsyncProcessRunning = false;

                                        if ((asyncOp as AsyncInstantiateOperation<GameObject>).Result.Length <= 0)
                                        {
                                            tileRequest.loadFailed = true;
                                            return;
                                        }

                                        GameObject tile  = (asyncOp as AsyncInstantiateOperation<GameObject>).Result[0];
                                        tileRequest.tile = tile;

                                        if (tileRoot == null) // This happens when the creation was initiated, but PIE was stopped and creation did not finish yet.
                                        {
                                            CleanRequest( tileRequest );
                                            tileRequests.Remove( name_ );
                                            return;
                                        }

                                        tile.name = name_;
                                        tile.transform.SetParent( tileRoot.transform, true );
                                        tile.transform.localPosition = Vector3.zero;
                                    };
                                };
                            };
                        }
                        else
                        {
                            // Update time so that we only unload the tile if for some seconds not desired anymore. To avoid flipping in and out.
                            tileRequest.time = unityTime; 
                        }
                    }
                    else  // Has children.
                    {
                        tile = children.First;
                        while (tile != null)
                        {
                            stack.Add( tile );
                            tile = tile.Next;
                        }
                    }
                }
            } while (stack.Count > 0);
        }

        void CleanRequest( BuildingTileRequest request )
        {
            if (request == null)
                return;
            if (request.isResourceLoadStarted)
            {
                Addressables.Release( request.resource );
            }
            request.tile.Destroy();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube( bounds.center, bounds.size );
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( StreamBuildings ) )]
    [InitializeOnLoad]
    public class StreamBuildingsEditor : Editor
    {
        bool toggle;
        public override void OnInspectorGUI()
        {
            StreamBuildings buildings = (StreamBuildings)target;


            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Generate in Editor"))
                {
                    EditorCoroutineUtility.StartCoroutine( buildings.BuildInEditor(), this );
                }
            }
            GUILayout.EndHorizontal();


            DrawDefaultInspector();
        }
    }

#endif
}