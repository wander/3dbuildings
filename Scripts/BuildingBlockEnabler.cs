using UnityEngine;
//using UnityEngine.Addressables; 

#if UNITY_EDITOR

namespace Wander
{
    public class BuildingBlockEnabler : MonoBehaviour
    {
        public Camera cam;

        float tileSize;
        float halfHeight;

        private void Start()
        {
            if ( cam == null )
            {
                cam = Camera.main;
            }

            var builder = FindFirstObjectByType<TerrainBuilder>();
            halfHeight = builder.terrainHeight/2;
            tileSize = (float)builder.tileSize;
        }

        private void Update()
        {
            if (cam == null)
            {
                return;
            }

            var planes = GeometryUtility.CalculateFrustumPlanes(cam);

            for (int i = 0; i < transform.childCount; i++)
            {
                var tile = transform.GetChild(i);
                var bounds = tile.GetComponent<MeshRenderer>().bounds;
                if (i == 0)
                {
                    MiscUtils.DrawBox( bounds.center, Quaternion.identity, bounds.size, Color.blue );
                }
                tile.gameObject.SetActive( GeometryUtility.TestPlanesAABB( planes, bounds ) );
            }
        }
    }
}

#endif