using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace Wander
{
    public class PostProcessCesium : MonoBehaviour
    {
        public GameObject cesiumRoot;
        public float height = 10;
        public float defheight = 10;
        public float normal = 0.7f;
        

        internal void PostProcess()
        {
            var renderers = cesiumRoot.GetComponentsInChildren<MeshFilter>( true );
            foreach( var r in renderers )
            {
                var vertices = r.sharedMesh.vertices;
                //var normals  = r.sharedMesh.normals;
                for (int k = 0;k < r.sharedMesh.subMeshCount;k++)
                {
                    var indices = r.sharedMesh.GetIndices(k);
                    for(int j = 0; j< indices.Length;j+=3)
                    {
                        var i0 = indices[j+0];
                        var i1 = indices[j+1];
                        var i2 = indices[j+2];
                        var v0 = r.transform.TransformPoint( vertices[i0] );
                        var v1 = r.transform.TransformPoint( vertices[i1] );
                        var v2 = r.transform.TransformPoint( vertices[i2] );
                        var n  = -Vector3.Cross( v1-v0, v2-v0).normalized;
                        if ( ((v0.y < height && v1.y < height && v2.y < height) && Mathf.Abs( n.y ) > normal ) ||
                                (v0.y < defheight && v1.y < defheight && v2.y < defheight) )
                        {
                            //if (  
                            {
                                indices[j+1] = indices[j+0];
                                indices[j+2] = indices[j+0];
                            }
                        }
                    }
                    r.sharedMesh.SetIndices( indices, MeshTopology.Triangles, k );
                }
                
            }
        }


        private void OnDrawGizmosSelected()
        {
        }

    }

#if UNITY_EDITOR

    [CustomEditor( typeof( PostProcessCesium ) )]
    [InitializeOnLoad]
    public class PostProcessCesiumEditor : Editor
    {
        public override void OnInspectorGUI()
        {
             var pp = (PostProcessCesium)target;

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Do it" ))
                {
                    pp.PostProcess();
                }
            }
            GUILayout.EndHorizontal();


            DrawDefaultInspector();
        }
    }

#endif
}

#endif