using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

#if UNITY_EDITOR

namespace Wander
{
    internal class B3dmTile
    {
        public UnityWebRequest www;
        public int numAttempts;
        public string name;
    }

    [ExecuteAlways()]
    public class CacheBuildings : MonoBehaviour
    {
        [ReadOnly] public CacheState state;
        [ReadOnly] public int numB3dms;
        [ReadOnly] public int numB3dmsFinished;
        [ReadOnly] public int numB3dmsFailed;
        [ReadOnly] public List<string> errorCodes;

        public string tilesetUrl = "https://3dbag.nl/download/3dtiles/v210908_fd2cee53/lod22/tileset.json";
        public string tilesUrl = "https://3dbag.nl/download/3dtiles/v210908_fd2cee53/lod22/";
        public int numSimultanousDownloads = 4;

        
        JToken tileset;
        UnityWebRequest tilesetRequest;
        List<B3dmTile> b3dmRequests;
        List<B3dmTile> b3dmStarted;

        public enum CacheState
        {
            None,
            RetrievingTileset,
            CreateB3dmRequests,
            DownloadingB3dms
        }

        internal void Cleanup()
        {
            state = CacheState.None;
            tileset = null;
            b3dmRequests = null;
            b3dmStarted = null;
            numB3dms = 0;
            numB3dmsFailed = 0;
            numB3dmsFinished = 0;
            errorCodes = null;
        }

        internal void Cache()
        {
            Cleanup();
            state = CacheState.RetrievingTileset;
            errorCodes = new List<string>();
        }

        void Update()
        {
            switch( state )
            {
                case CacheState.None:
                    break;

                case CacheState.RetrievingTileset:
                    DownloadTilset();
                    break;

                case CacheState.CreateB3dmRequests:
                    CreateB3dmRequests();
                    break;

                case CacheState.DownloadingB3dms:
                    DownloadB3dms();
                    break;
            }
        }

        void DownloadTilset()
        {
            if (!Directory.Exists( Path.Combine( Application.streamingAssetsPath, "Buildings" ).NormalizePath() ))
            {
                Directory.CreateDirectory( Path.Combine( Application.streamingAssetsPath, "Buildings" ).NormalizePath() );
            }
            if (!Directory.Exists( Path.Combine( Application.streamingAssetsPath, "Buildings/tiles" ).NormalizePath() ))
            {
                Directory.CreateDirectory( Path.Combine( Application.streamingAssetsPath, "Buildings", "tiles" ).NormalizePath() );
            }

            if (File.Exists( Path.Combine( Application.streamingAssetsPath, "Buildings/tileset.json" ).NormalizePath()))
            {
                if (tileset == null)
                {
                    var data = File.ReadAllBytes( Path.Combine( Application.streamingAssetsPath, "Buildings/tileset.json" ).NormalizePath() );
                    var json = JObject.Parse( Encoding.UTF8.GetString(data) );
                    tileset  = json["root"];
                }
                state = CacheState.CreateB3dmRequests;
                return;
            }

            if (tilesetRequest != null)
            {
                if (tilesetRequest.result == UnityWebRequest.Result.InProgress)
                    return;

                if (tilesetRequest.result == UnityWebRequest.Result.Success)
                {
                    var data = tilesetRequest.downloadHandler.data;
                    var json = JObject.Parse( Encoding.UTF8.GetString(data) );
                    tileset  = json["root"];
                    File.WriteAllBytesAsync( Path.Combine( Application.streamingAssetsPath, "Buildings/tileset.json" ).NormalizePath(), data );
                    state = CacheState.CreateB3dmRequests;
                    return;
                }

                Debug.LogWarning( tilesetRequest.error );
            }
            tilesetRequest = UnityWebRequest.Get( tilesetUrl );
            tilesetRequest.SendWebRequest();
        }
        
        void CreateB3dmRequests()
        {
            if (tileset==null)
            {
                Debug.LogError( "No tileset downloaded" );
                state = CacheState.None;
                return;
            }

            b3dmRequests = new List<B3dmTile>();
            List<JToken> stack = new List<JToken>();
            stack.Add( tileset );
            do
            {
                var tile = stack[stack.Count-1];
                stack.RemoveAt( stack.Count-1 ); // O(1)

                var children = tile["children"];
                if (children == null) // isLeaf
                {
                    var b3dmFilename = tile["content"]["uri"].ToString();
                    var diskPath = Application.streamingAssetsPath.Combine( "Buildings", b3dmFilename ).NormalizePath();
                    if (!File.Exists(diskPath))
                    {
                        var uri   = tilesUrl.Combine(b3dmFilename);
                        var b3dm  = new B3dmTile();
                        b3dm.www  = UnityWebRequest.Get( uri );
                        b3dm.name = b3dmFilename;
                        b3dmRequests.Add( b3dm );
                    }
                }
                else  // has children
                {
                    tile = children.First;
                    while (tile != null)
                    {
                        stack.Add( tile );
                        tile = tile.Next;
                    }
                }
                
            } while (stack.Count > 0);

            // Compose initial list
            b3dmStarted = new List<B3dmTile>();
            numB3dms    = b3dmRequests.Count;
            int count   = Mathf.Min(numSimultanousDownloads, b3dmRequests.Count);
            for (int i = 0; i < count ;i++)
            {
                b3dmStarted.Add( b3dmRequests[0] );
                b3dmRequests[0].www.SendWebRequest();
                b3dmRequests.RemoveAt( 0 );
            }
            state = CacheState.DownloadingB3dms;
        }

        void DownloadB3dms()
        {
            if(b3dmStarted==null)
            {
                state = CacheState.None;
                return;
            }

            for (int i = 0; i < b3dmStarted.Count; i++ )
            {
                var web = b3dmStarted[i].www;
                if (web.isDone)
                {
                    bool startNew = true;
                    if (web.result == UnityWebRequest.Result.Success)
                    {
                        var diskPath = Application.streamingAssetsPath.Combine( "Buildings", b3dmStarted[i].name ).NormalizePath();
                        File.WriteAllBytesAsync( diskPath, web.downloadHandler.data );
                        numB3dmsFinished++;
                        b3dmStarted.RemoveAt( i );
                        i--;
                    }
                    else if (b3dmStarted[i].numAttempts < 3)
                    {
                        startNew = false;
                        b3dmStarted[i].numAttempts++;
                        b3dmStarted[i].www = UnityWebRequest.Get( b3dmStarted[i].www.uri );
                        b3dmStarted[i].www.SendWebRequest();
                    }
                    else
                    {
                        numB3dmsFailed++;
                        if (!errorCodes.Contains( b3dmStarted[i].www.error ))
                            errorCodes.Add( b3dmStarted[i].www.error );
                        b3dmStarted.RemoveAt( i );
                        i--;
                    }

                    // Add new one if there are left overs.
                    if (startNew && b3dmRequests.Count != 0)
                    {
                        b3dmStarted.Add( b3dmRequests[0] );
                        b3dmRequests.RemoveAt( 0 );
                        b3dmStarted[b3dmStarted.Count-1].www.SendWebRequest();
                    }
                }
            }
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( CacheBuildings ) )]
    [InitializeOnLoad]
    public class CacheBuildingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            CacheBuildings buildings = (CacheBuildings)target;

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Cache" ))
                {
                    buildings.Cache();
                }

                if (GUILayout.Button( "Stop & clean" ))
                {
                    buildings.Cleanup();
                }
            }
            GUILayout.EndHorizontal();


            DrawDefaultInspector();

        }
    }

#endif
}

#endif